function myMap(cb) {
    const results = [];

    for (let i = 0; i < this.length; i++) {
        results.push(cb(this[i], i, this));
    }

    return results;
}

function myReduce(cb, initialValue) {
    let acc;
    let curr;

    if (!this.length && !initialValue)
        throw new Error("Can't reduce on empty array, provide initial value");
    else {
        acc = initialValue ? initialValue : this[0];
        for (let i = 1; i < this.length; i++) {
            curr = this[i];
            acc = cb(acc, curr, i, this);
        }
    }
    return acc;
}

module.exports = function () {
    if (!Array.prototype.myMap) {
        Array.prototype.myMap = myMap
    }

    if (!Array.prototype.myReduce) {
        Array.prototype.myReduce = myReduce
    }
}